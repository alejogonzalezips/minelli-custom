-- CreateTable
CREATE TABLE "CustomCollections" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "collectionID" TEXT NOT NULL,
    "discount" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "CustomCollections_collectionID_key" ON "CustomCollections"("collectionID");
