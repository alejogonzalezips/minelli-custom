/*
  Warnings:

  - Added the required column `endDate` to the `CustomCollections` table without a default value. This is not possible if the table is not empty.
  - Added the required column `startDate` to the `CustomCollections` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA defer_foreign_keys=ON;
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_CustomCollections" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "collectionID" TEXT NOT NULL,
    "tag" TEXT NOT NULL,
    "discount" INTEGER NOT NULL,
    "startDate" DATETIME NOT NULL,
    "endDate" DATETIME NOT NULL
);
INSERT INTO "new_CustomCollections" ("collectionID", "discount", "id", "tag") SELECT "collectionID", "discount", "id", "tag" FROM "CustomCollections";
DROP TABLE "CustomCollections";
ALTER TABLE "new_CustomCollections" RENAME TO "CustomCollections";
CREATE UNIQUE INDEX "CustomCollections_collectionID_key" ON "CustomCollections"("collectionID");
CREATE UNIQUE INDEX "CustomCollections_tag_key" ON "CustomCollections"("tag");
PRAGMA foreign_keys=ON;
PRAGMA defer_foreign_keys=OFF;
