-- CreateTable
CREATE TABLE "CollectionByTitle" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "collectionId" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "keywordList" TEXT NOT NULL,
    "productCount" INTEGER NOT NULL DEFAULT 0,
    "collectionLink" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- CreateIndex
CREATE UNIQUE INDEX "CollectionByTitle_collectionId_key" ON "CollectionByTitle"("collectionId");
