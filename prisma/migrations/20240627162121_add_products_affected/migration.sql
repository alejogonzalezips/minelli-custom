-- RedefineTables
PRAGMA defer_foreign_keys=ON;
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_CustomCollections" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "collectionID" TEXT NOT NULL,
    "tag" TEXT NOT NULL,
    "discount" INTEGER NOT NULL,
    "startDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "endDate" DATETIME,
    "productsAffected" INTEGER NOT NULL DEFAULT 0
);
INSERT INTO "new_CustomCollections" ("collectionID", "discount", "endDate", "id", "startDate", "tag") SELECT "collectionID", "discount", "endDate", "id", "startDate", "tag" FROM "CustomCollections";
DROP TABLE "CustomCollections";
ALTER TABLE "new_CustomCollections" RENAME TO "CustomCollections";
CREATE UNIQUE INDEX "CustomCollections_collectionID_key" ON "CustomCollections"("collectionID");
CREATE UNIQUE INDEX "CustomCollections_tag_key" ON "CustomCollections"("tag");
PRAGMA foreign_keys=ON;
PRAGMA defer_foreign_keys=OFF;
