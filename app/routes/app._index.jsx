import { useEffect, useState } from "react";
import { useFetcher } from "@remix-run/react";
import { Page, Layout, Card, Button, TextField, Checkbox, Badge } from "@shopify/polaris";
import { TitleBar, useAppBridge } from "@shopify/app-bridge-react";

export default function CreateCollectionPage() {
  const fetcher = useFetcher();
  const appBridge = useAppBridge();
  const [tag, setTag] = useState("");
  const [discount, setDiscount] = useState(false);
  const [discountValue, setDiscountValue] = useState("");
  const [startDate, setStartDate] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endDate, setEndDate] = useState("");
  const [endTime, setEndTime] = useState("");
  const [collections, setCollections] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchCollections = async () => {
    const response = await fetch("/app/getcollections");
    if (response.ok) {
      const data = await response.json();
      setCollections(data);
    }
  };

  const createCollection = async () => {
    setLoading(true);
    const formData = new FormData();
    formData.append("tag", tag);
    formData.append("discount", discount);
    if (discount) {
      formData.append("discountValue", discountValue);
    } else {
      formData.append("discountValue", 0);
    }
    formData.append("startDate", startDate);
    formData.append("startTime", startTime);
    formData.append("endDate", endDate);
    formData.append("endTime", endTime);

    const response = await fetch("/app/createcollection", {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      const { collectionId } = await response.json();
      fetchCollections();

      if (appBridge) {
        appBridge.Toast.create(appBridge).show("Collection created successfully");
      }
    }
    setLoading(false);
  };

  const deleteCollection = async (collectionId, discount, tag) => {
    setLoading(true);
    const formData = new FormData();
    formData.append("collectionId", collectionId);
    formData.append("discount", discount);
    formData.append("tag", tag);
    const response = await fetch("/app/deletecollection", {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      fetchCollections();

      if (appBridge) {
        appBridge.Toast.create(appBridge).show("Collection deleted successfully");
      }
    }
    setLoading(false);
  };

  const refreshCollection = async (collectionId, discountValue, tag) => {
    setLoading(true);
    const formData = new FormData();
    formData.append("collectionId", collectionId);
    formData.append("discount", discountValue);
    formData.append("tag", tag);
    const response = await fetch("/app/refreshcollection", {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      fetchCollections();

      if (appBridge) {
        appBridge.Toast.create(appBridge).show("Collection refreshed successfully");
      }
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchCollections();
  }, []);

  const getCollectionStatus = (startDate, endDate) => {
    const now = new Date();
    const start = new Date(startDate);
    const end = endDate ? new Date(endDate) : null;

    if (now < start) {
      return "Not Published";
    } else if (end && now >= start && now <= end) {
      return "Active";
    } else if (!end) {
      return "Active";
    } else {
      return "Expired";
    }
  };

  return (
    <Page>
      <TitleBar title="Create Collection with Discount" />
      <Layout>
        {loading && <div className="loader">Loading...</div>}
        <Layout.Section>
          <Card sectioned>
            <TextField
              label="Tag"
              value={tag}
              onChange={(value) => setTag(value)}
            />
            <Checkbox
              label="Apply Discount"
              checked={discount}
              onChange={(newChecked) => setDiscount(newChecked)}
            />
            {discount && (
              <TextField
                label="Discount Value (%)"
                type="number"
                value={discountValue}
                onChange={(value) => setDiscountValue(Number(value))}
              />
            )}
            <div style={{ display: 'flex', gap: '10px' }}>
              <TextField
                label="Start Date"
                type="date"
                value={startDate}
                onChange={(value) => setStartDate(value)}
              />
              <TextField
                label="Start Time"
                type="time"
                value={startTime}
                onChange={(value) => setStartTime(value)}
              />
            </div>
            <div style={{ display: 'flex', gap: '10px' }}>
              <TextField
                label="End Date"
                type="date"
                value={endDate}
                onChange={(value) => setEndDate(value)}
              />
              <TextField
                label="End Time"
                type="time"
                value={endTime}
                onChange={(value) => setEndTime(value)}
              />
            </div>
            <Button onClick={createCollection} primary fullWidth>
              Create Collection
            </Button>
          </Card>
        </Layout.Section>
        <Layout.Section>
          {collections.length > 0 && (
            <Card sectioned>
              <h2>Created Collections</h2>
              <ul style={{ padding: 0, listStyleType: "none" }}>
                {collections.map((collection) => {
                  const status = getCollectionStatus(collection.startDate, collection.endDate);
                  return (
                    <li key={collection.collectionID} style={{ marginBottom: '1rem', borderBottom: '1px solid #e1e3e5', paddingBottom: '1rem' }}>
                      <div>
                        <p><strong>Tag:</strong> {collection.tag}</p>
                        <p><strong>Discount Applied:</strong> {collection.discount}%</p>
                        <p><strong>Start Date:</strong> {new Date(collection.startDate).toLocaleString()}</p>
                        <p><strong>End Date:</strong> {collection.endDate ? new Date(collection.endDate).toLocaleString() : 'No End Date'}</p>
                        <p><strong>Products Affected:</strong> {collection.productsAffected}</p>
                        {collection.collectionID.startsWith("placeholder") && (
                          <p><em>(Pending)</em></p>
                        )}
                        <div style={{ marginTop: '0.5rem', display: 'flex', gap: '10px', alignItems: 'center' }}>
                          {status === "Not Published" && (
                            <Badge status="warning">Not Published</Badge>
                          )}
                          {status === "Active" && (
                            <Button
                              onClick={() => refreshCollection(collection.collectionID, collection.discount, collection.tag)}
                              primary
                            >
                              Refresh
                            </Button>
                          )}
                          {status === "Expired" && (
                            <Badge status="critical">Expired</Badge>
                          )}
                          <Button
                            onClick={() => deleteCollection(collection.collectionID, collection.discount, collection.tag)}
                            destructive
                          >
                            Delete
                          </Button>
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </Card>
          )}
        </Layout.Section>
      </Layout>
    </Page>
  );
}
