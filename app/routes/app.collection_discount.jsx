import { useEffect, useState } from "react";
import { useFetcher } from "@remix-run/react";
import { Page, Layout, Card, Button, TextField, Checkbox } from "@shopify/polaris";
import { TitleBar, useAppBridge } from "@shopify/app-bridge-react";

export default function CreateCollectionPage() {
  const fetcher = useFetcher();
  const appBridge = useAppBridge();
  const [tag, setTag] = useState("");
  const [discount, setDiscount] = useState(false);
  const [discountValue, setDiscountValue] = useState("");
  const [startDate, setStartDate] = useState("") || new Date().toISOString().split('T')[0]; // Default to today
  const [startTime, setStartTime] = useState("") || new Date().toTimeString().split(' ')[0];
  const [endDate, setEndDate] = useState("");
  const [endTime, setEndTime] = useState("");
  const [collections, setCollections] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchCollections = async () => {
    const response = await fetch("/app/getcollections");
    if (response.ok) {
      const data = await response.json();
      setCollections(data);
    }
  };

  const createCollection = async () => {
    setLoading(true);
    const formData = new FormData();
    formData.append("tag", tag);
    formData.append("discount", discount);
    if (discount) {
      formData.append("discountValue", discountValue);
    } else {
      formData.append("discountValue", 0);
    }
    formData.append("startDate", startDate);
    formData.append("startTime", startTime);
    formData.append("endDate", endDate);
    formData.append("endTime", endTime);

    const response = await fetch("/app/createcollection", {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      const { collectionId } = await response.json();
      fetchCollections();

      if (appBridge) {
        appBridge.Toast.create(appBridge).show("Collection created successfully");
      }
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchCollections();
  }, []);

  return (
    <Page>
      <TitleBar title="Create Collection with Discount" />
      <Layout>
        {loading && <div className="loader">Loading...</div>}
        <Layout.Section>
          <Card sectioned>
            <TextField
              label="Tag"
              value={tag}
              onChange={(value) => setTag(value)}
            />
            <Checkbox
              label="Apply Discount"
              checked={discount}
              onChange={(newChecked) => setDiscount(newChecked)}
            />
            {discount && (
              <TextField
                label="Discount Value (%)"
                type="number"
                value={discountValue}
                onChange={(value) => setDiscountValue(Number(value))}
              />
            )}
            <TextField
              label="Start Date"
              type="date"
              value={startDate}
              onChange={(value) => setStartDate(value)}
            />
            <TextField
              label="Start Time"
              type="time"
              value={startTime}
              onChange={(value) => setStartTime(value)}
            />
            <TextField
              label="End Date"
              type="date"
              value={endDate}
              onChange={(value) => setEndDate(value)}
            />
            <TextField
              label="End Time"
              type="time"
              value={endTime}
              onChange={(value) => setEndTime(value)}
            />
            <Button onClick={createCollection} primary>
              Create Collection
            </Button>
          </Card>
        </Layout.Section>
        <Layout.Section>
          {collections.length > 0 && (
            <Card sectioned>
              <h2>Created Collections</h2>
              <ul>
                {collections.map((collection) => (
                  <li key={collection.collectionID}>
                    Tag: {collection.tag}{" "}
                    || Discount Applied: {collection.discount}{" % "}
                    <Button onClick={() => deleteCollection(collection.collectionID, collection.discount, collection.tag)} destructive>
                      Delete
                    </Button>
                  </li>
                ))}
              </ul>
            </Card>
          )}
        </Layout.Section>
      </Layout>
    </Page>
  );
}
