import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import db from "../db.server";
import schedule from 'node-schedule';
import { deleteCollection } from "../utils/deleteCollection";
import { scheduledJobs } from "../job.scheduler";

export const loader = async ({ request }) => {
  await authenticate.admin(request);
  return null;
};

export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request);
  const formData = await request.formData();
  const tag = formData.get("tag");
  const discount = formData.get("discount") === "true";
  const discountValue = parseInt(formData.get("discountValue"));
  const startDate = formData.get("startDate");
  const startTime = formData.get("startTime");
  const endDate = formData.get("endDate");
  const endTime = formData.get("endTime");
  let delete_cron_id = "";
  
  try {
    const local_tag = await db.customCollections.findFirst({
      where: { tag: tag },
    });
    if (local_tag != null) {
      return json({ success: false, error: "Collection already exists" }, { status: 500 });
    }

    const startDateTime = startDate && startTime ? new Date(`${startDate}T${startTime}`) : new Date();
    const endDateTime = endDate && endTime ? new Date(`${endDate}T${endTime}`) : null;
    const placeholderID = `placeholder-${Date.now()}`;
    delete_cron_id = placeholderID;

    await db.customCollections.create({
      data: {
        collectionID: placeholderID,
        tag: tag,
        discount: discountValue != null ? discountValue : 0,
        startDate: startDateTime,
        endDate: endDateTime,
        productsAffected: 0,  // Initialize with 0
      }
    });

    const createCollectionAndApplyDiscount = async () => {
      const response = await admin.graphql(
        `#graphql
        mutation CollectionCreate($input: CollectionInput!) {
          collectionCreate(input: $input) {
            userErrors {
              field
              message
            }
            collection {
              id
              title
              ruleSet {
                appliedDisjunctively
                rules {
                  column
                  relation
                  condition
                }
              }
            }
          }
        }`,
        {
          variables: {
            input: {
              title: `Collection for ${tag}`,
              ruleSet: {
                appliedDisjunctively: false,
                rules: [
                  {
                    column: "TAG",
                    relation: "EQUALS",
                    condition: `${tag}`
                  }
                ]
              },
            },
          },
        }
      );

      const body = await response.json();
      const collectionId = body.data.collectionCreate.collection.id;
      delete_cron_id = collectionId;
      let productsAffected = 0;

      // Update the placeholder ID with the actual collection ID
      await db.customCollections.update({
        where: { collectionID: placeholderID },
        data: { collectionID: collectionId },
      });

      if (discount) {
        let hasNextPage = true;
        let cursor = null;

        while (hasNextPage) {
          const productsResponse = await admin.graphql(
            `#graphql
            query {
              products(first: 100, query: "tag:${tag}", after: ${cursor}) {
                pageInfo {
                  hasNextPage
                  endCursor
                }
                edges {
                  node {
                    id
                    title
                    variants(first: 100) {
                      edges {
                        node {
                          id
                          price
                          compareAtPrice
                        }
                      }
                    }
                  }
                }
              }
            }`
          );

          const productsData = await productsResponse.json();
          const products = productsData.data.products.edges;
          productsAffected += products.length;
          hasNextPage = productsData.data.products.pageInfo.hasNextPage;
          cursor = '"' + productsData.data.products.pageInfo.endCursor + '"';

          for (const product of products) {
            for (const variantEdge of product.node.variants.edges) {
              const variant = variantEdge.node;
              const newPrice = (parseFloat(variant.price) * (1 - discountValue / 100)).toFixed(2);
              const newCompareAtPrice = variant.price;

              await admin.graphql(
                `#graphql
                mutation ProductVariantUpdate($input: ProductVariantInput!) {
                  productVariantUpdate(input: $input) {
                    productVariant {
                      id
                      price
                      compareAtPrice
                    }
                  }
                }`,
                {
                  variables: {
                    input: {
                      id: variant.id,
                      price: newPrice,
                      compareAtPrice: newCompareAtPrice,
                    },
                  },
                }
              );
            }
          }
        }
      }

      await db.customCollections.update({
        where: { collectionID: collectionId },
        data: { productsAffected },
      });
    };

    if (startDateTime > new Date()) {
      const createJob = schedule.scheduleJob(startDateTime, createCollectionAndApplyDiscount);
      scheduledJobs.set(delete_cron_id, { createJob });

      if (endDateTime) {
        const deleteJob = schedule.scheduleJob(endDateTime, async () => {
          await deleteCollection(admin, delete_cron_id, discount, tag);
        });
        scheduledJobs.get(delete_cron_id).deleteJob = deleteJob;
      }
    } else {
      await createCollectionAndApplyDiscount();

      if (endDateTime) {
        const deleteJob = schedule.scheduleJob(endDateTime, async () => {
          await deleteCollection(admin, delete_cron_id, discount, tag);
        });
        scheduledJobs.set(delete_cron_id, { deleteJob });
      }
    }

    return json({ success: true });
  } catch (error) {
    console.error(error);
    return json({ success: false, error: error.message }, { status: 500 });
  }
};
