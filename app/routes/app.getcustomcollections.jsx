import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import { TitleBar, useAppBridge } from "@shopify/app-bridge-react";
import db from "../db.server"


export const loader = async ({ request }) => {
    await authenticate.admin(request);
  
    try {
      const collections = await db.collectionByTitle.findMany({
        orderBy: {
          createdAt: 'desc',
        },
      });
      return json({ success: true, collections });
    } catch (error) {
      console.error(error);
      return json({ success: false, error: error.message }, { status: 500 });
    }
  };
  