import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import db from "../db.server";

export const loader = async ({ request }) => {
  await authenticate.admin(request);
  return null;
};

export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request);
  const formData = await request.formData();
  const discount = parseInt(formData.get("discount"));
  const collectionId = formData.get("collectionId");
  const tag = formData.get("tag");
  let hasNextPage = true;
  let cursor = null;
  let productsAffected = 0;

  try {

    if (discount!= 0) {
      
    
        while (hasNextPage) {
            const productsResponse_erase = await admin.graphql(
              `#graphql
              query{
                products(first: 100, query: "tag:${tag}", after: ${cursor}) {
                  pageInfo {
                    hasNextPage
                    endCursor
                  }
                  edges {
                    node {
                      id
                      title
                      variants(first: 100) {
                        edges {
                          node {
                            id
                            price
                            compareAtPrice
                          }
                        }
                      }
                    }
                  }
                }
              }`
            );
    
      const productsData_erase = await productsResponse_erase.json();
      const products_erase = productsData_erase.data.products.edges;
      hasNextPage = productsData_erase.data.products.pageInfo.hasNextPage;
      cursor = '"' + productsData_erase.data.products.pageInfo.endCursor + '"';
      // Update product prices
      for (const product of products_erase) {
        for (const variantEdge of product.node.variants.edges) {
          const variant = variantEdge.node;
          const newPrice = variant.compareAtPrice;
          const newCompareAtPrice = null; 
    
          await admin.graphql(
            `#graphql
            mutation ProductVariantUpdate($input: ProductVariantInput!) {
              productVariantUpdate(input: $input) {
                productVariant {
                  id
                  price
                  compareAtPrice
                }
              }
            }`,
            {
              variables: {
                input: {
                  id: variant.id,
                  price: newPrice,
                  compareAtPrice: newCompareAtPrice,
                },
              },
            }
          );
        }
      }
    }
    cursor =null;
    hasNextPage = true;
    while (hasNextPage) {
        const productsResponse_recreate = await admin.graphql(
          `#graphql
          query{
            products(first: 100, query: "tag:${tag}", after: ${cursor}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              edges {
                node {
                  id
                  title
                  variants(first: 100) {
                    edges {
                      node {
                        id
                        price
                        compareAtPrice
                      }
                    }
                  }
                }
              }
            }
          }`
        );
    
      const productsData_recreate = await productsResponse_recreate.json();
      const products_recreate = productsData_recreate.data.products.edges;
      productsAffected += products_recreate.length;

      hasNextPage = productsData_recreate.data.products.pageInfo.hasNextPage;
      cursor = '"' + productsData_recreate.data.products.pageInfo.endCursor + '"';
      // Update product prices
      for (const product of products_recreate) {
        for (const variantEdge of product.node.variants.edges) {
          const variant = variantEdge.node;
          const newPrice = (parseFloat(variant.price) * (1-(discount/100)).toFixed(2)); 
          const newCompareAtPrice = variant.price; 
    
          await admin.graphql(
            `#graphql
            mutation ProductVariantUpdate($input: ProductVariantInput!) {
              productVariantUpdate(input: $input) {
                productVariant {
                  id
                  price
                  compareAtPrice
                }
              }
            }`,
            {
              variables: {
                input: {
                  id: variant.id,
                  price: newPrice,
                  compareAtPrice: newCompareAtPrice,
                },
              },
            }
          );
        }
      }
    }

    
    
    
    
    }
    await db.customCollections.update({
      where: { collectionID: collectionId },
      data: { productsAffected },
    });






    return json({ success: true });
  } catch (error) {
    console.error(error);
    return json({ success: false, error: error.message }, { status: 500 });
  }
};
