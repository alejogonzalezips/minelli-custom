import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import db from "../db.server";

export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request);
  const formData = await request.formData();
  const keywords = formData.get("keywords");
  const customTitle = formData.get("title");

  if (!keywords || !customTitle) {
    return json({ success: false, error: "Keywords or custom title missing" }, { status: 400 });
  }

  const keywordGroups = keywords.split(",").map(group => group.trim());

  if (keywordGroups.length === 0) {
    return json({ success: false, error: "Invalid keywords" }, { status: 400 });
  }

  try {
    // Create the title filter query
    const queryConditions = keywordGroups.map(group => {
      const words = group.split('_').map(word => `*${word.trim()}*`);
      return `(title:${words.join(" AND title:")})`;
    });

    const queryString = queryConditions.join(" OR ");
    let products = [];
    let cursor = null;
    let hasNextPage = true;

    while (hasNextPage) {
      const productsResponse = await admin.graphql(
        `#graphql
        query($cursor: String) {
          products(first: 100, after: $cursor, query: "${queryString}") {
            edges {
              cursor
              node {
                id
                title
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }`,
        { cursor }
      );

      const data = await productsResponse.json();
      const edges = data.data.products.edges;
      products = products.concat(edges.map(edge => edge.node));
      hasNextPage = data.data.products.pageInfo.hasNextPage;
      cursor = data.data.products.pageInfo.endCursor;
    }

    if (products.length === 0) {
      return json({ success: false, error: "No products match the keywords" }, { status: 404 });
    }

    const createCollectionResponse = await admin.graphql(
      `#graphql
      mutation($input: CollectionInput!) {
        collectionCreate(input: $input) {
          collection {
            id
            legacyResourceId
          }
          userErrors {
            field
            message
          }
        }
      }`,
      {
        variables: {
          input: {
            title: customTitle,
            products: products.map(product => product.id),
          },
        },
      }
    );

    const collectionData = await createCollectionResponse.json();
    const collectionId = collectionData.data.collectionCreate.collection.id;
    const legacyResourceId = collectionData.data.collectionCreate.collection.legacyResourceId;

    const shopresponse = await admin.graphql(`
    #graphql
    query{
      shop {
        myshopifyDomain 
      }
    }
  `);

    const shopdata = await shopresponse.json();
    const shopDomain = shopdata.data.shop.myshopifyDomain;
    const cleanShopDomain = shopDomain.replace('.myshopify.com', '');

    const collectionLink = `https://admin.shopify.com/store/${cleanShopDomain}/collections/${legacyResourceId}`;

    await db.collectionByTitle.create({
      data: {
        collectionId: collectionId,
        title: customTitle,
        keywordList: keywords,
        productCount: products.length,
        collectionLink: collectionLink,
      },
    });

    return json({ success: true, collectionId });
  } catch (error) {
    console.error(error);
    return json({ success: false, error: error.message }, { status: 500 });
  }
};
