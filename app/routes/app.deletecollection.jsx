import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import { deleteCollection } from "../utils/deleteCollection";

export const loader = async ({ request }) => {
  await authenticate.admin(request);
  return null;
};

export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request);
  const formData = await request.formData();
  const collectionId = formData.get("collectionId");
  const discount = parseInt(formData.get("discount"));
  const tag = formData.get("tag");

  const result = await deleteCollection(admin, collectionId, discount, tag);

  if (result.success) {
    return json({ success: true });
  } else {
    return json({ success: false, error: result.error }, { status: 500 });
  }
};
