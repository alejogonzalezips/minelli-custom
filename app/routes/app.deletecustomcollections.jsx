import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import db from "../db.server";

export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request); // Ensure admin is properly authenticated
  const formData = await request.formData();
  const id = formData.get("id");

  if (!id) {
    return json({ success: false, error: "No collection ID provided" }, { status: 400 });
  }

  try {
    // Find the collection in your database
    const collection = await db.collectionByTitle.findUnique({
      where: { id: parseInt(id, 10) },
    });

    if (!collection) {
      return json({ success: false, error: "Collection not found" }, { status: 404 });
    }

    // Delete collection from Shopify
    const deleteResponse = await admin.graphql(
      `#graphql
      mutation collectionDelete($input: CollectionDeleteInput!) {
        collectionDelete(input: $input) {
          deletedCollectionId
          userErrors {
            field
            message
          }
        }
      }`,
      {
        variables: {
          input: {
            id: collection.collectionId, // Assuming this is the Shopify Collection ID
          },
        },
      }
    );

    const deleteResult = await deleteResponse.json();

    if (deleteResult.data.collectionDelete.userErrors.length > 0) {
      return json({ success: false, error: deleteResult.data.collectionDelete.userErrors[0].message }, { status: 400 });
    }

    // Delete collection from your database
    await db.collectionByTitle.delete({
      where: { id: parseInt(id, 10) },
    });

    return json({ success: true });
  } catch (error) {
    console.error(error);
    return json({ success: false, error: error.message }, { status: 500 });
  }
};
