import { useState, useEffect } from "react";
import { Page, Layout, Card, Button, TextField, Spinner, Banner, Link } from "@shopify/polaris";
import { useAppBridge } from "@shopify/app-bridge-react";

export default function CustomCollectionAndManage() {
  const appBridge = useAppBridge();

  // States for Custom Collection Creation
  const [title, setTitle] = useState("");
  const [keywords, setKeywords] = useState("");
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  // States for Managing Collections
  const [collections, setCollections] = useState([]);
  const [loadingCollections, setLoadingCollections] = useState(true);
  const [deleteSuccess, setDeleteSuccess] = useState(false);
  const [error, setError] = useState("");

  // Function to Create a Collection
  const createCollection = async () => {
    setLoading(true);
    try {
      const formData = new FormData();
      formData.append("title", title);
      formData.append("keywords", keywords);

      const response = await fetch("/app/customcollectioncreate", {
        method: "POST",
        body: formData,
      });

      if (response.ok) {
        if (appBridge) {
          appBridge.Toast.create(appBridge).show("Collection created successfully");
        }
        setSuccess(true);
        fetchCollections();
      } else {
        throw new Error("Failed to create collection");
      }
    } catch (err) {
      console.error(err);
      if (appBridge) {
        appBridge.Toast.create(appBridge).show("Failed to create collection");
      }
    } finally {
      fetchCollections();
      setLoading(false);
    }
    fetchCollections();

    
  };

  // Function to Fetch Collections
  const fetchCollections = async () => {
    setLoadingCollections(true);
    try {
      const response = await fetch("/app/getcustomcollections");
      if (response.ok) {
        const data = await response.json();
        setCollections(data.collections || []);
        
      } else {
        throw new Error("Failed to fetch collections");
      }
    } catch (err) {
      setError(err.message);
    } finally {
      setLoadingCollections(false);
    }
  };

  // Function to Delete a Collection
  const deleteCollection = async (id) => {
    setLoading(true);
    try {
      const formData = new FormData();
      formData.append("id", id);

      const response = await fetch("/app/deletecustomcollections", {
        method: "POST",
        body: formData,
      });

      if (response.ok) {
        setDeleteSuccess(true);
        fetchCollections();
      } else {
        throw new Error("Failed to delete collection");
      }
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
    fetchCollections();
  };

  // Fetch collections on component mount
  useEffect(() => {
    fetchCollections();
  }, []);

  // Define banner height and spacing
  const BANNER_HEIGHT = 60; // Approximate height in pixels
  const BANNER_SPACING = 10; // Spacing between banners in pixels

  // Calculate the number of visible banners
  const visibleBannersCount = [success, deleteSuccess, error].filter(Boolean).length;

  // Calculate total top padding based on visible banners
  const totalTopPadding = visibleBannersCount * (BANNER_HEIGHT + BANNER_SPACING);

  return (
    <Page>
      {/* Fixed Banner Container */}
      <div style={{ position: "fixed", top: 0, left: 0, width: "100%", zIndex: 2000 }}>
        {success && (
          <Banner
            status="success"
            title="Collection created successfully"
            onDismiss={() => setSuccess(false)}
            style={{ marginBottom: `${BANNER_SPACING}px` }}
          />
        )}
        {deleteSuccess && (
          <Banner
            status="success"
            title="Collection deleted successfully"
            onDismiss={() => setDeleteSuccess(false)}
            style={{ marginBottom: `${BANNER_SPACING}px` }}
          />
        )}
        {error && (
          <Banner
            status="critical"
            title={error}
            onDismiss={() => setError("")}
            style={{ marginBottom: `${BANNER_SPACING}px` }}
          />
        )}
      </div>

      {/* Main Content with Top Padding */}
      <div style={{ paddingTop: `${totalTopPadding}px` }}>
        <Layout>
          {/* Section for Creating Custom Collection */}
          <Layout.Section>
            <Card sectioned>
              <TextField
                label="Collection Title"
                value={title}
                onChange={(value) => setTitle(value)}
                placeholder="Enter a custom title for the collection"
              />
              <TextField
                label="Keywords (comma-separated)"
                value={keywords}
                onChange={(value) => setKeywords(value)}
                multiline
              />
              <Button
                onClick={createCollection}
                primary
                fullWidth
                disabled={!title || !keywords || loading}
                loading={loading}
              >
                Create Collection
              </Button>
            </Card>
          </Layout.Section>

          {/* Section for Managing Collections */}
          <Layout.Section>
            {loadingCollections ? (
              <Spinner size="large" />
            ) : (
              <Card sectioned>
                {collections.length === 0 ? (
                  <p>No collections found</p>
                ) : (
                  collections.map((collection) => (
                    <div
                      key={collection.id}
                      style={{
                        marginBottom: "20px",
                        borderBottom: "1px solid #e1e3e5",
                        paddingBottom: "20px",
                      }}
                    >
                      <p>
                        <strong>Title:</strong> {collection.title}
                      </p>
                      <p>
                        <strong>Keyword List:</strong> {collection.keywordList}
                      </p>
                      <p>
                        <strong>Product Count:</strong> {collection.productCount}
                      </p>
                      <p>
                        <strong>Link:</strong>{" "}
                        <Link url={collection.collectionLink}>View Collection</Link>
                      </p>
                      <Button onClick={() => deleteCollection(collection.id)} destructive>
                        Delete Collection
                      </Button>
                    </div>
                  ))
                )}
              </Card>
            )}
          </Layout.Section>
        </Layout>

        {/* Full-Screen Spinner during Operations */}
        {(loading || loadingCollections) && (
          <div
            style={{
              position: "fixed",
              top: 0,
              left: 0,
              width: "100vw",
              height: "100vh",
              backgroundColor: "rgba(0, 0, 0, 0.5)",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              zIndex: 1000,
            }}
          >
            <Spinner size="large" />
          </div>
        )}
      </div>
    </Page>
  );
}
