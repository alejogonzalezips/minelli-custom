import { json } from "@remix-run/node";
import { authenticate } from "../shopify.server";
import { TitleBar, useAppBridge } from "@shopify/app-bridge-react";
import db from "../db.server"

export const loader = async ({ request }) => {
    await authenticate.admin(request);
    const collectionsCreated = await db.customCollections.findMany();
    return json(collectionsCreated);
  };
  
export const action = async ({ request }) => {
    const collectionsCreated = await db.customCollections.findMany();

    return json(collectionsCreated);
  }