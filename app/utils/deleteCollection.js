import db from "../db.server";
import { authenticate } from "../shopify.server";
import { scheduledJobs } from "../job.scheduler";

export async function deleteCollection(admin, collectionId, discount, tag) {
  let hasNextPage = true;
  let cursor = null;
  const jobs = scheduledJobs.get(collectionId);

  try {
    // Check if the collection still exists
    const collection = await db.customCollections.findUnique({
      where: { collectionID: collectionId },
    });

    if (!collection) {
      console.log(`Collection with ID ${collectionId} does not exist.`);
      return { success: false, error: "Collection does not exist" };
    }

      // If the collection ID is a placeholder, delete it without further actions
      if (collectionId.startsWith("placeholder")) {
        await db.customCollections.delete({
          where: { collectionID: collectionId },
        });
        if (jobs) {
          jobs.createJob?.cancel();
          jobs.deleteJob?.cancel();
          scheduledJobs.delete(collectionId);}
        return { success: true };
      }

    if (discount !== 0) {
      while (hasNextPage) {
        const productsResponse = await admin.graphql(
          `#graphql
          query {
            products(first: 100, query: "tag:${tag}", after: ${cursor}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              edges {
                node {
                  id
                  title
                  variants(first: 100) {
                    edges {
                      node {
                        id
                        price
                        compareAtPrice
                      }
                    }
                  }
                }
              }
            }
          }`
        );

        const productsData = await productsResponse.json();
        const products = productsData.data.products.edges;
        hasNextPage = productsData.data.products.pageInfo.hasNextPage;
        cursor = '"' + productsData.data.products.pageInfo.endCursor + '"';

        for (const product of products) {
          for (const variantEdge of product.node.variants.edges) {
            const variant = variantEdge.node;
            const newPrice = variant.compareAtPrice;
            const newCompareAtPrice = null;

            await admin.graphql(
              `#graphql
              mutation ProductVariantUpdate($input: ProductVariantInput!) {
                productVariantUpdate(input: $input) {
                  productVariant {
                    id
                    price
                    compareAtPrice
                  }
                }
              }`,
              {
                variables: {
                  input: {
                    id: variant.id,
                    price: newPrice,
                    compareAtPrice: newCompareAtPrice,
                  },
                },
              }
            );
          }
        }
      }
    }

    await admin.graphql(
      `#graphql
      mutation collectionDelete($input: CollectionDeleteInput!) {
        collectionDelete(input: $input) {
          deletedCollectionId
          shop {
            id
            name
          }
          userErrors {
            field
            message
          }
        }
      }`,
      {
        variables: {
          input: {
            id: `${collectionId}`
          }
        },
      }
    );

    await db.customCollections.delete({
      where: { collectionID: collectionId },
    });

    return { success: true };
  } catch (error) {
    console.error(error);
    return { success: false, error: error.message };
  }
}
